//Pham Hoang Son - 695105106
#include <bits/stdc++.h>
using namespace std;

int main() {
	int d, m, y, c, k, dowtemp, mtemp, ytemp;
	string sp = "/";
	string dow;
	cout << "-----TINH NGAY TRONG TUAN-----";
	d = m = y = 0;
	cout << endl << "Ngay:\t"; cin >> d;
	while(d <= 0 || d > 31) {
		cout << "\t[Err]Ngay khong hop le!\nNgay:\t"; cin >> d;
	}
	cout << "Thang:\t"; cin >> m;
	while(m <= 0 || m > 12) {
		cout << "Thang khong hop le!\nThang:\t"; cin >> m;
	}
	while((d > 29 && m == 2)||(d > 30 && m == 4)||(d > 30 && m == 6)||(d > 30 && m == 9)||(d > 30 && m == 11)) {
		cout << "\t[Err]Thang " << m << " khong co ngay " << d << "!\n";
		cout << "Ngay:\t"; cin >> d;
		cout << "Thang:\t"; cin >> m;
	}
	cout << "Nam:\t"; cin >> y;
	while(y <= 0 || y > 9999) {
		cout << "\t[Err]Nam khong hop le!\nNam:\t"; cin >> y;
	}
	while(((y%4 == 0 && y%400 != 0)||(y%4 != 0))&&(d >= 29 && m == 2)) {
		cout << "Ngay da nhap: " << d << sp << m << sp << y << " khong hop le vi " << y << " khong phai nam nhuan!";
		cout << endl << "Ngay:\t"; cin >> d;
		while(d <= 0 || d > 31) {
			cout << "\t[Err]Ngay khong hop le!\nNgay:\t"; cin >> d;
		}
		cout << "Thang:\t"; cin >> m;
		while(m <= 0 || m > 12) {
			cout << "Thang khong hop le!\nThang:\t"; cin >> m;
		}
		while((d > 29 && m == 2)||(d > 30 && m == 4)||(d > 30 && m == 6)||(d > 30 && m == 9)||(d > 30 && m == 11)) {
			cout << "\t[Err]Thang " << m << " khong co ngay " << d << "!\n";
			cout << "Ngay:\t"; cin >> d;
			cout << "Thang:\t"; cin >> m;
		}
		cout << "Nam:\t"; cin >> y;
		while(y <= 0 || y > 9999) {
			cout << "\t[Err]Nam khong hop le!\nNam:\t"; cin >> y;
		}
	}
	if(m == 1) {
		ytemp = y - 1;
		mtemp = 13;
		c = int(ytemp / 100);
		k = ytemp - (c*100);
		dowtemp = (d + ((26*(mtemp + 1))/10) + k + (k/4) + (c/4) + 5*c) % 7;
	} else if(m == 2) {
		ytemp = y - 1;
		mtemp = 14;
		c = int(ytemp / 100);
		k = ytemp - (c*100);
		dowtemp = (d + ((26*(mtemp + 1))/10) + k + (k/4) + (c/4) + 5*c) % 7;
	} else {
		c = int(y / 100);
//		if(y > c*100) {
//			c = c + 1;
//		}
		k = y - (c*100);
		dowtemp = (d + ((26*(m + 1))/10) + k + (k/4) + (c/4) + 5*c) % 7;
	}
	switch (dowtemp){
		case 0: dow = "Thu 7"; break;
		case 1: dow = "Chu Nhat"; break;
		case 2: dow = "Thu 2"; break;
		case 3: dow = "Thu 3"; break;
		case 4: dow = "Thu 4"; break;
		case 5: dow = "Thu 5"; break;
		case 6: dow = "Thu 6"; break;
	}
	cout << "Ngay da nhap: " << dow << ", " << d << sp << m << sp << y << ".\n";
	system("pause");
}
